""""Wrapper script for clang-tidy which returns a score."""
import sys
import re
from cmake_pc_hooks.clang_tidy import ClangTidyCmd


class ClangTidyScoreCmd(ClangTidyCmd):
    """Class for the clang-tidy command."""

    @staticmethod
    def _parse_err(stderr):
        err = stderr.split("\n")
        matching = [s for s in err if "generated" in s]
        if matching:
            warnings = int(re.search(r"\d+(?=\s*warning)", matching[0]).group(0) or 0)
            errors = int(re.search(r"\d+(?=\s*error)", matching[0]).group(0) or 0)
            return 5 * errors + warnings
        return 0


    def _parse_output(self, result):
        """
        Parse output and check whether some errors occurred.
        Args:
            result (namedtuple): Result from calling a command
        Returns:
            False if no errors were detected, True in all other cases.
        """

        score = self._parse_err(result.stderr)
        # Reset stderr if it's complaining about problems in system files
        if result.stdout and "non-user code" not in result.stderr:
            pass
        else:
            result.stderr = ""

        print(f"Error-Score is {score}")

        return score > 0 or result.returncode != 0


def main():
    """
    Run command.
    Args:
        argv (:obj:`list` of :obj:`str`): list of arguments
    """
    cmd = ClangTidyScoreCmd(sys.argv[1:])
    cmd.run()


if __name__ == "__main__":
    main()
