"""Wrapper script for clang-tidy which returns a score."""
VERSION = (1, 0, 0)
# string created from tuple to avoid inconsistency
__version__ = ".".join([str(x) for x in VERSION])
